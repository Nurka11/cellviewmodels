//
//  InlineConfigurable.swift
//  ChocoDevCon
//
//  Created by Sultan Seidalin on 9/21/19.
//  Copyright © 2019 Aviata LLC. All rights reserved.
//

import Foundation
import UIKit

protocol InlineConfigurable {}

extension InlineConfigurable {
    
    @discardableResult func configured(_ configurator: (Self) -> Void) -> Self  {
        // Run the provided configurator:
        configurator(self)
        
        // Return self (which is now configured):
        return self
    }
}

extension NSObject: InlineConfigurable {}

extension UIView {
    
    class var identifier: String {
        return String(describing: self)
    }
    
    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
}

extension UIColor {
    
    class func color(_ hexCode: String) -> UIColor {
        return UIColor.color(hexCode, alpha: 1.0)
    }
    
    class func color(_ hexCode: String, alpha: CGFloat) -> UIColor {
        var hex = hexCode
        
        if hex.hasPrefix("#") {
            let index = hex.index(hex.startIndex, offsetBy: 1)
            hex = String(hex[index...])
        }
        
        guard hex.count == 6 else {
            return UIColor.gray
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: hex).scanHexInt32(&rgbValue)
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
        let blue = CGFloat(rgbValue & 0x0000FF) / 255.0
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
