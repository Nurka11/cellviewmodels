//
//  TotalSumCell.swift
//  ChocoDevCon
//
//  Created by Sultan Seidalin on 9/21/19.
//  Copyright © 2019 Aviata LLC. All rights reserved.
//

import UIKit

class TotalSumCell: UITableViewCell, CellViewModelConfigurable {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var ticketPriceLabel: UILabel!
    @IBOutlet private weak var insurancePriceLabel: UILabel!
    @IBOutlet private weak var totalPriceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setup()
    }
    
    private func setup() {
        containerView.configured {
            $0.layer.cornerRadius = 8
            $0.layer.masksToBounds = true
        }
        
        shadowView.configured {
            $0.layer.cornerRadius = 8
            $0.clipsToBounds = false
            $0.layer.shadowColor = UIColor.black.withAlphaComponent(alpha).cgColor
            $0.layer.shadowOpacity = Float(0.3)
            $0.layer.shadowOffset = CGSize(width: 0, height: 2)
            $0.layer.shadowRadius = 4
        }
    }
    
    func configure(with viewModel: CellViewModel) {
        guard let viewModel = viewModel as? TotalSumCellViewModel else { return }
        
        ticketPriceLabel.text = String(viewModel.ticketPrice)
        insurancePriceLabel.text = String(viewModel.insurancePrice)
        totalPriceLabel.text = String(viewModel.totalPrice)
    }
}
