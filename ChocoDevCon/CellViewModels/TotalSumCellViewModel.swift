//
//  TotalSumCellViewModel.swift
//  ChocoDevCon
//
//  Created by Sultan Seidalin on 10/11/19.
//  Copyright © 2019 Aviata LLC. All rights reserved.
//

import Foundation

class TotalSumCellViewModel : CellViewModel {
    
    var ticketPrice: Int = 23000
    
    var insurancePrice: Int = 1500
    
    var totalPrice: Int {
        return ticketPrice + insurancePrice
    }
}
