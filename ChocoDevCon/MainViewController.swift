//
//  ViewController.swift
//  ChocoDevCon
//
//  Created by Sultan Seidalin on 9/21/19.
//  Copyright © 2019 Aviata LLC. All rights reserved.
//

import UIKit

//enum Section: Int {
//    case auth = 0
//    case booking
//    case totalSum
//    case all
//}
//
//func numberOfSections(in tableView: UITableView) -> Int {
//    return Section.all.rawValue
//}
//
//func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    return 1
//}

class MainViewController: UIViewController {
    
    private var tableView = UITableView()
    private let cellViewModels = [BookingCellViewModel(),
                                  TotalSumCellViewModel(),
                                  BookingCellViewModel(),
                                  AuthCellViewModel(),
                                  BookingCellViewModel()] as [CellViewModel]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        tableView.configured {
            $0.dataSource = self
            $0.contentInset = UIEdgeInsets(top: 8.0, left: 0.0, bottom: 8.0, right: 0.0)
            $0.separatorStyle = .none
            $0.tableFooterView = UIView()
            $0.backgroundColor = UIColor.color("F9F9FB")
            $0.register(AuthCell.nib, forCellReuseIdentifier: AuthCellViewModel.identifier)
            $0.register(TotalSumCell.nib, forCellReuseIdentifier: TotalSumCellViewModel.identifier)
            $0.register(BookingCell.nib, forCellReuseIdentifier: BookingCellViewModel.identifier)
        }
        view = tableView
    }
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = cellViewModels[indexPath.row]
        let cellIdentifier = cellViewModel.identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CellViewModelConfigurable
        cell.configure(with: cellViewModel)
        return cell as! UITableViewCell
    }
}
