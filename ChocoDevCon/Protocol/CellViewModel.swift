//
//  CellViewModel.swift
//  ChocoDevCon
//
//  Created by Sultan Seidalin on 10/11/19.
//  Copyright © 2019 Aviata LLC. All rights reserved.
//

import Foundation

protocol CellViewModel {
    
    static var identifier: String { get }
    
    var identifier: String { get }
}

extension CellViewModel {
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var identifier: String {
        return String(describing: Self.self)
    }
    
}
