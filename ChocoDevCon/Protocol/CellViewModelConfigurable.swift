//
//  CellViewModelConfigurable.swift
//  ChocoDevCon
//
//  Created by Sultan Seidalin on 10/11/19.
//  Copyright © 2019 Aviata LLC. All rights reserved.
//

import Foundation

protocol CellViewModelConfigurable {
    
    func configure(with viewModel: CellViewModel)
    
}
